<?php
declare(strict_types=1);

namespace App\Listeners;

use App\Models\Model;
use App\Models\Post;
use Illuminate\Events\Dispatcher;

class CropWordsFromStopListSubscriber
{
    protected array $models = [
        Post::class,
    ];

    private array $words = [
        'bad',
        'word'
    ];

    private array $fields = [
        'content'
    ];

    public function subscribe(Dispatcher $events): void
    {
        foreach ($this->models as $model) {
            $events->listen('eloquent.saving: ' . $model, [$this, 'handle']);
        }
    }

    public function handle(Model $model): void
    {
        foreach (array_keys($model->getDirty()) as $field) {
            if (in_array($field, $this->fields)) {
                $model->{$field} = str_replace($this->words, '', $model->{$field});
            }
        }
    }
}
