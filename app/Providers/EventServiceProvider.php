<?php
declare(strict_types=1);

namespace App\Providers;

use App\Listeners\CropWordsFromStopListSubscriber;
use App\Models\Comment;
use App\Observers\CropWordsFromStopListObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    protected $subscribe = [
        CropWordsFromStopListSubscriber::class
    ];

    protected $observers = [
        Comment::class => [
            CropWordsFromStopListObserver::class,
        ]
    ];

    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
