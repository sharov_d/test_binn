<?php

namespace App\Observers;

use App\Models\Model;

class CropWordsFromStopListObserver
{
    private array $words = [
        'bad',
        'word'
    ];

    private array $fields = [
        'content'
    ];

    public function saving(Model $model): void
    {
        $this->crop($model);
    }

    public function crop(Model $model): void
    {
        foreach (array_keys($model->getDirty()) as $field) {
            if (in_array($field, $this->fields)) {
                $model->{$field} = str_replace($this->words, '', $model->{$field});
            }
        }
    }
}
